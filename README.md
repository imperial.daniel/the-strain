# The Strain

## Description
This is a Caves of Qud mod, for version 2.0.201.114. I was inspired on the fungi and tried to create something that enabled skills and abilities. Actually, there was a lot of inspiration from Warframe's Nidus (go check it out, great game).

The mod enables a new morphotype called 'The Strain'. As every morphotype, it comes with advantages and drawbacks. You may lose some reputation with the Consortium of Phyta, but you gain incredible power:

- It allows you to, once every 10 turns, do a ranged melee attack in the form of the Impale skill.
- When you reach level 5, your body gets further infected by the strain! You lose part of your body to the fungi, but you gain immense power instead.

Please note this mod is very much a work in progress, and I made it in way to learn more about C#. It may be simple at the moment, but I'm thinking about adding a bunch of new features, like:

- New effects at max mutation Level;
- Custom fungi interactions;
- New powers over time;

## Known Bugs
So, this is, of course, WIP. So it has some bugs. Please report them to me as soon as you find them, and I'll try and fix them.

## Acknowledgements
Thanks a lot for my friends Aulberon, Gus and Zelun for inspiring me into taking this endeavor. This is my first 'finished' project, and there shall be more to come.

