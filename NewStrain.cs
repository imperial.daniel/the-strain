using System.Collections.Generic;
using System.Linq;
using System;
using XRL.Core;
using XRL.Language;
using XRL.Rules;
using XRL.UI;
using XRL.World.Capabilities;
using XRL.World.Effects;
using XRL.World.Parts.Mutation;
using XRL.World.Parts;
using XRL.World;
using XRL;

namespace XRL.World.Parts.Mutation
{
    [Serializable]
    public class Strain : BaseDefaultEquipmentMutation
    {
      public GameObject FungiObject;

      public string BodyPartType = "Hands";

      public bool CreateObject = true;

      public string ManagerID => ParentObject.id + "::Strain";

      [NonSerialized]
      private static List<string> variants = new List<String> {"Hands", "Face" ,"Feet"};

      public Guid StrainActivatedAbilityID = Guid.Empty;
      public Guid GreaterStrainActivatedAbilityID = Guid.Empty;

      public override IPart DeepCopy(GameObject Parent, Func<GameObject, GameObject> MapInv)
      {
        Strain obj = base.DeepCopy(Parent, MapInv) as Strain;
        obj.FungiObject = null;
        return obj;
      }

      public Strain()
      {
        DisplayName = "Fungal Strain";
        MaxLevel = 8;
      }

      public override bool GeneratesEquipment()
      {
        return true;
      }

    //	public override bool WantEvent(int ID, int cascade)
    //	{
    //		if (!base.WantEvent(ID, cascade) && ID != AfterGameLoadedEvent.ID)
    //		{
    //			return ID == PartSupportEvent.ID;
    //		}
    //		return true;
    //	}

      public bool CheckInfestedLimbExists()
      {
          if (!CreateObject) {return true;}
          if (HasRegisteredSlot (BodyPartType)) {return GetRegisteredSlot(BodyPartType, evenIfDismembered: false) != null;}
          return false;
      }
      //todo
    //	public override bool HandleEvent(PartSupportEvent E)
    //	{
    //		if (E.Skip != this && IsMyActivatedAbilityToggledOn(EnableActivatedAbilityID))
    //		{
    //			return false;
    //		}
    //		return base.HandleEvent(E);
    //	}

      public int GetRange(int Level)
        {
            return 8 + Level * 2;
        }

      public bool Cast(Strain mutation = null, string level = "5-6")
        {
            if (mutation == null)
            {
                mutation = new Strain();
                mutation.Level = Stat.Roll(level);
                mutation.ParentObject = XRLCore.Core.Game.Player.Body;
            }
                Cell targetCell = mutation.PickDestinationCell(GetRange(base.Level),
                                                AllowVis.Any,
                                                Locked: true,
                                          IgnoreSolid: false,
                                             IgnoreLOS: true,
                                         RequireCombat: true,
                               PickTarget.PickStyle.EmptyCell,
                                                         null,
                                                 Snap: true);
            if (targetCell == mutation.ParentObject.CurrentCell && mutation.ParentObject.IsPlayer() && Popup.ShowYesNoCancel("Are you sure you want to target " + mutation.ParentObject.itself + "?") != 0)
            {
              return false;
            }
            BodyPart registeredSlot = GetRegisteredSlot(BodyPartType, evenIfDismembered: true);
            if (base.Level >= 5)
            {
                mutation.CooldownMyActivatedAbility(mutation.GreaterStrainActivatedAbilityID, 5);
                mutation.UseEnergy(500, "Physical Mutation Strain");
            }
            else
            {
                mutation.CooldownMyActivatedAbility(mutation.StrainActivatedAbilityID, 15);
                mutation.UseEnergy(1000, "Physical Mutation Strain");

            }
            IComponent<GameObject>.XDidY(mutation.ParentObject, "spur", "vicious tendrils" + ((registeredSlot != null) ? (" from " + mutation.ParentObject.its + " " + registeredSlot.GetOrdinalName()) : ""), "!", null, mutation.ParentObject);
            Impale(targetCell);
            return true;
        }

      public void Impale(Cell C)
        {
            string dice = GetWeaponDamage(base.Level);
            if (base.Level >= 5)
            {
                dice += "+8";
            }
            DieRoll cachedDieRoll = dice.GetCachedDieRoll();
            if (C != null)
            {
                foreach (GameObject target in C.GetObjectsInCell())
                {
                    if (!target.IsFlying || base.Level >= 5){
                    target.TakeDamage(cachedDieRoll.Resolve(),
                                         "from the tendrils!",
                                                   "Bleeding",
                                                         null,
                                                         null,
                                                 ParentObject,
                                                         null,
                                                         null,
                                            Accidental: false,
                                         Environmental: false,
                                              Indirect: false,
                                        ShowUninvolved: false,
                                      ShowForInanimate: false,
                                      SilentIfNoDamage: false,
                                     ParentObject.GetPhase());
                    }
                }
            }
        }

      public override void Register(GameObject Object)
      {
        Object.RegisterPartEvent(this, "CommandEvokeTendrils");
        Object.RegisterPartEvent(this, "AIGetOffensiveMutationList");
        Object.RegisterPartEvent(this, "StrainEvolution");
        base.Register(Object);
      }
      //todo
      public override bool FireEvent(Event E)
      {
          if (E.ID == "AIGetOffensiveMutationList")
          {
              if (CheckInfestedLimbExists() 
                      && E.GetIntParameter("Distance") <= GetRange(base.Level) 
                      && IsMyActivatedAbilityAIUsable(StrainActivatedAbilityID) 
                      && ParentObject.HasLOSTo(E.GetGameObjectParameter("Target"),
                          IncludeSolid: true,
                          UseTargetability: true))
              {
                  E.AddAICommand("CommandEvokeTendrils");
              }
          }
          else if (E.ID == "CommandEvokeTendrils")
          {
              if (!CheckInfestedLimbExists())
              {
                if (ParentObject.IsPlayer())
                {
                  Popup.ShowFail("Your " + BodyPartType + " is too damaged to do that!");
                }
                return false;
              }
              if (!Cast(this))
              {
                return false;
              }
            }
            return base.FireEvent(E);
      }

      public int GetPenetration(int Level)
      {
        if (Level < 5)
        {
          return 1;
        }
        if (Level < 8)
        {
          return 2;
        }
        return 3;
      }

      public int GetAV(int Level)
      {
        if (Level < 3)
        {
          return 1;
        }
        if (Level < 5)
        {
          return 2;
        }
        if (Level < 7)
        {
          return 3;
        }
        return 4;
      }

      public override string GetDescription()
      {
        if (Level < 5)
        {
            BodyPart registeredSlot = GetRegisteredSlot(BodyPartType, evenIfDismembered: true);
            if (registeredSlot != null)
            {
                return "An assymetric but somewhat sharp tonfa summoned from your " + registeredSlot.GetOrdinalName() + "." + "\n{{cyan|+200}} Reputation with {{cyan|fungi}}.";
            }
            return "You possess a strain of the fungi inside you, which manifests as a weapon in your body." + "\n{{cyan|+200}} Reputation with {{cyan|fungi}}.";
        }
        else
        {
            return "A fully articulate, refined fungic blade rising from your body, working itself as a substitute for your (no longer present) limb.\n" + "\n{{cyan|+400}} Reputation with {{cyan|fungi}}.";
        }
      }

      public override string GetLevelText(int Level)
      {
        BodyPart registeredSlot = GetRegisteredSlot(BodyPartType, evenIfDismembered: true);
        if (Level < 5)
        {
            if (registeredSlot != null)
            {
                return "Evokes a fungal tendril from the ground by planting your "
                    + registeredSlot.GetOrdinalName()
                    + " on the floor, impaling the enemy.\n"
                    + "\n{{cyan|+200}} Reputation with {{cyan|fungi}}.\n"
                    + "\n Cooldown: 15 rounds"
                    + "\n Damage: "
                    + GetWeaponDamage(Level);
            }
            return "Evokes a fungal tendril from the ground by planting your "
                + "protrusion"
                + " on the floor, impaling the enemy.\n"
                + "\n{{cyan|+200}} Reputation with {{cyan|fungi}}.\n"
                + "\nCooldown: 15 rounds"
                + "\nDamage: "
                + "{{rules|" + GetWeaponDamage(Level);
        }
        return "Evokes a fungal tendril from the ground by planting your "
            + "protrusion"
            + " on the floor, impaling the enemy.\n"
            + "\n{{cyan|+400}} Reputation with {{cyan|fungi}}.\n"
            + "\nCooldown: 15 rounds"
            + "\nDamage: "
            + "{{rules|" + GetWeaponDamage(Level);
      }

      public string GetWeaponDamage(int Level)
      {
          string damage, final;
          int mod;
          if (Level <= 2)
          {
              mod = Level;
              damage = "d4";
          }
          else
          {
              mod = (Level % 2 == 0 && Level > 6) ? 2 : 1; 
              if (Level < 4) {damage = "d6";} else
              if (Level < 6) {damage = "d8";} else
              {damage = "d10";} 
//            switch (Level)
//            {
//                case < 4:
//                    damage = "d6";
//                    break;
//                case < 6:
//                    damage = "d8";
//                    break;
//                case < 8:
//                    damage = "d10";
//                    break;
//                case < 10:
//                    damage = "d12";
//                    break;
//                default:
//                    damage ="d14";
//                    break;
//            }
          }
          final = mod + damage;
          return final;
      }

      public void StrainEvolve()
      {
          if (base.Level > 1 && ParentObject.IsPlayer())
          {
              Popup.Show("The fungi strain inside of you grows stronger.",
                                                   Capitalize: true);
              ParentObject.pPhysics.PlayWorldSound("FungalInfectionAcquired", PitchVariance: 2f);
          }
          if (base.Level == 5)
          {
              if (ParentObject.IsPlayer())
              {
                  Popup.Show("Your body gets further infected by the strain!");
              }
              Body SourceBody = ParentObject.GetPart("Body") as Body;
              List<BodyPart> infestedLimbs = InfestedLimbs(SourceBody);
              // uma solução horrível mas veremos como funciona
              List<BodyPart> infestedPassive = InfestedPassive(infestedLimbs);
              List<BodyPart> infestedActive = InfestedActive(infestedLimbs);
              BodyPart CurrentBody = SourceBody.GetBody();
              // crie uma lista de partes extras pra serem usadas se for necessário
              // List<BodyPart> appendages = new List<BodyPart>();
              // resolvido usando as cláusulas corretas na função InfestedLimbs();
              foreach (BodyPart limb in infestedActive)
              {
                  SourceBody.Dismember(limb);
              }
              foreach (BodyPart limb in infestedPassive)
              {
                  SourceBody.Dismember(limb);
              }
              switch (BodyPartType)
                  // testando colocar "fungal" depois do ManagerID pra ver se é a categoria
                  // não, é a mobilidade do membro
                  // importante colocar também função pra alterar a lateralidade dos membros
                  // por ora melhor só impedir usar múltiplos braços e pernas (fica muito roubado;
                  // usar duas cabeças é de boa)
                  {
                  case "Hands":
                      // raptelium = raptorious + artelium (articulum)
                      // vou fazer uma solução super porca pro multiple arms, mas é isso aí
                      for (int i = 1; i < infestedPassive.Count && i < 3; i++)
                      {
                          BodyPart ArmTemplate = CurrentBody.AddPartAt("Fungal Raptelium", 2, null, null, null, null, ManagerID + infestedLimbs.Count, 17, null, null, null, null, null, null, null, null, null, null, null, "Arm", new string [4]{"Hands", "Feet", "Roots", "Thrown Weapon"});
                          ArmTemplate.AddPart("Fungal Claw", 2, null, "Fungal Claws", null, null, ManagerID + infestedLimbs.Count, 17);
                          CurrentBody.AddPartAt(ArmTemplate, "Fungal Raptelium", 1, null, null, null, null, ManagerID + infestedLimbs.Count, 17).AddPart("Fungal Claw", 1, null, "Fungal Claws", null, null, ManagerID + infestedLimbs.Count, 17);
                          CurrentBody.AddPartAt("Fungal Claws", 0, null, null, "Fungal Claws", null, ManagerID + infestedLimbs.Count + 1, 17, null, null, null, null, null, null, null, null, null, null, null, "Hands", new string[3]{"Feet", "Roots", "Thrown Weapon"});
                      }
                      break;
                  case "Feet":
                      // revisar isso aqui depois
                      // if (infestedLimbs.Find(x => x.Type.Contains("Leg")))
                      for (int i = 0; i < infestedActive.Count; i++)
                      {
                          BodyPart LegTemplate = CurrentBody.AddPartAt("Fungal Tarsus", 0, null, null, null, null, ManagerID + infestedLimbs.Count, 17, null, null, null, null, null, null, null, null, null, null, null, "Leg", new string [3]{"Feet", "Roots", "Thrown Weapon"});
                          LegTemplate.AddPart("Fungal Exopodite", 0, null, "Fungal Exopodites", null, null, ManagerID + infestedLimbs.Count, 17);
                          CurrentBody.AddPartAt("Fungal Exopodites", 0, null, null, "Fungal Exopodites", null, ManagerID + infestedLimbs.Count + 1, 17, null, null, null, null, null, null, null, null, null, null, null, "Feet", new string[2]{"Roots", "Thrown Weapon"});
                      }
                      break;
                  case "Face":
                      if (infestedActive.Count == 1)
                      {
                          CurrentBody.AddPartAt("Fungal Arista", 0, null, null, null, null, ManagerID + infestedLimbs.Count + 1, 0, null, null, null, null, null, null, null, null, null, null, null, "Face", new string [5]{"Body", "Back", "Arm", "Hands", "Thrown Weapon"});
                      }
                      else for (int i = 1; i <= infestedActive.Count; i++)
                      {
                          CurrentBody.AddPartAt("Fungal Arista", i, null, null, null, null, ManagerID + infestedLimbs.Count + 1, 0, null, null, null, null, null, null, null, null, null, null, null, "Face", new string [5]{"Body", "Back", "Arm", "Hands", "Thrown Weapon"});
                      }
                      break;
                  }
              int registeredSlotID = GetRegisteredSlot(BodyPartType, evenIfDismembered: true).ID;
              RegisteredSlots.Remove(BodyPartType, out registeredSlotID);
              List<BodyPart> infestedParts = new List<BodyPart>();
              CurrentBody.FindByManager(ManagerID + infestedLimbs.Count, infestedParts);
              if (BodyPartType == "Face")
              {
                  CurrentBody.FindByManager(ManagerID + infestedLimbs.Count + 1, infestedParts);
              }
              if (ParentObject.IsPlayer() && infestedParts != null)
              {
                  Popup.Show("Your body grows fungal parts where you have been dismembered!");
                  foreach (BodyPart infected in infestedParts)
                  {
                      Popup.Show("The strain grew a " + infected.Name + "!");
                  }
              }
              BodyPart newRegisteredSlot = CurrentBody.FindByManager(ManagerID + infestedLimbs.Count + 1);
              RegisterSlot(BodyPartType, newRegisteredSlot);
          }
      }

    	public override List<string> GetVariants()
    	{
    		return variants;
    	}
    
    	public override void SetVariant(int n)
    	{
    		if (n < variants.Count)
    		{
    			BodyPartType = variants[n];
    		}
    		else
    		{
    			BodyPartType = variants[0];
    		}
    		base.SetVariant(n);
    	}

      public List<BodyPart> InfestedLimbs(Body body)
      {
            List<BodyPart> infestedLimbs = new List<BodyPart>();
            switch (BodyPartType)
            {
                case "Hands":
                    infestedLimbs = (from p in body.GetParts()
                            where (p.Type == "Hand" || p.Type == "Arm")
                            select p).ToList();
                            break;
                case "Feet":
                    infestedLimbs = (from p in body.GetParts()
                            where (p.Type == "Feet" || p.Type == "Leg")
                            select p).ToList();
                            break;
                case "Face":
                    infestedLimbs = (from p in body.GetParts()
                            where p.Type == "Face"
                            select p).ToList();
                            break;
            }
            return infestedLimbs;
      }

      public List<BodyPart> InfestedActive (List<BodyPart> InfestedLimbs)
      {
          List<BodyPart> infestedActive = new List<BodyPart>();
          infestedActive = (from p in InfestedLimbs 
                    where (p.Type == "Hand" || p.Type == "Feet" || p.Type == "Face")
                    select p).ToList();
          return infestedActive;
      }

      public List<BodyPart> InfestedPassive (List<BodyPart> InfestedLimbs)
      {
          List<BodyPart> infestedPassive = new List<BodyPart>();
          infestedPassive = (from p in InfestedLimbs 
                    where (p.Type == "Arm" || p.Type == "Leg")
                    select p).ToList();
          return infestedPassive;
      }

      public override void OnRegenerateDefaultEquipment(Body body)
      {
        BodyPart bodyPart;
        if (CreateObject)
        {
            List<BodyPart> infestedLimbs = InfestedLimbs(body);
            // cria partes ativas e passivas pra ficar mais fácil de controlar
            // List<BodyPart> infestedActive = infestedLimbs.Find(x => x.Type.ContainsAny(new [] {"Hand", "Feet", "Face"}));
            // List<BodyPart> infestedPassive = infestedLimbs.Find(x => x.Type.ContainsAny(new [] {"Arm", "Leg"}));
            List<BodyPart> infestedActive = InfestedActive(infestedLimbs);
            List<BodyPart> infestedPassive = InfestedPassive(infestedLimbs);
            if (!HasRegisteredSlot(BodyPartType) && infestedLimbs != null)
            {
                // registra apenas na mão direita (primeira mão, preciso colocar em duas)
                // simples, só registrar o slot como as mãos mesmo
                // RegisterSlot(BodyPartType, infestedLimbs[0]);
                RegisterSlot(BodyPartType, body.GetFirstPart(BodyPartType));
            }
            else
            {
                bodyPart = GetRegisteredSlot(BodyPartType, evenIfDismembered: false);
            }
            for (int i = 0; i < infestedActive.Count; i++)
            {
                // força desequipar itens 
                //ParentObject.FireEvent(Event.New("CommandForceUnequipObject", "BodyPart", infestedActive[i]).SetSilent(Silent: true));
                infestedActive[i].DefaultBehavior = GameObject.create("Fungal Protrusion");
                infestedActive[i].DefaultBehavior.GetPart<MeleeWeapon>().BaseDamage = GetWeaponDamage(base.Level);
                infestedActive[i].DefaultBehavior.GetPart<MeleeWeapon>().PenBonus = GetPenetration(base.Level);
                infestedActive[i].DefaultBehavior.GetPart<Armor>().AV = GetAV(base.Level);
            }
            // cria o evento pra forçar equipar os escudos nos braços/pernas
            Event ForceEquip = Event.New("CommandForceEquipObject");
            ForceEquip.SetParameter("Object", GameObject.create("Fungal Mesopleuron"));
            ForceEquip.SetSilent(Silent: true);
            if (base.Level >= 5 && infestedPassive != null)
            {
                switch (BodyPartType)
                {
                    case "Feet":
                        body.GetPartsByManager(ManagerID + infestedLimbs.Count, infestedPassive);
                        break;
                    case "Face":
                        foreach (BodyPart part in body.GetPart("Head")) { infestedPassive.Add(part); }
                        break;
                }
                foreach (BodyPart limb in infestedPassive)
                {
//                  limb.Equipped?.UnequipAndRemove();
                    ForceEquip.SetParameter("BodyPart", limb);
                    ParentObject.FireEvent(ForceEquip);
//                  limb.Equip(GameObject.create("Fungal Mesopleuron"));
//                  limb.DefaultBehaviorBlueprint = "Fungal Mesopleuron";
//                  limb.DefaultBehavior = GameObject.create("Fungal Mesopleuron");
                }
            }
        }
      }

      public override bool ChangeLevel(int NewLevel)
      {
        StrainEvolve();
//      if (this.FungiObject != null)
//      {
//          Armor part = this.FungiObject.GetPart<Armor>();
//          part.AV = this.GetAV(NewLevel);
//      }
        if (base.Level == 5)
        {
            RemoveMyActivatedAbility(ref StrainActivatedAbilityID);
            GreaterStrainActivatedAbilityID = AddMyActivatedAbility("Greater Impale", "CommandEvokeTendrils", "Physical Mutation", "\a", null);
            XRL.Core.XRLCore.Core.Game.PlayerReputation.modify("Fungi", 200, because: "due to your allegiance to the fungi");
            XRL.Core.XRLCore.Core.Game.PlayerReputation.modify("Consortium", -200, because: "due to your allegiance to the fungi");
            XRL.Core.XRLCore.Core.Game.PlayerReputation.modify("", -200, because: "due to your allegiance to the fungi");
        }
        return base.ChangeLevel(NewLevel);
      }

      public override bool CanLevel()
      {
          return true;
      }

      public override bool Mutate(GameObject GO, int Level)
      {
        StrainActivatedAbilityID = AddMyActivatedAbility("Impale", "CommandEvokeTendrils", "Physical Mutation", "\a", null);
        if (CreateObject)
        {
            //acredito que esse seja o jeito "legacy", mas por enquanto vou usar ele
            // XRL.Core.XRLCore.Core.Game.PlayerReputation.modify("Fungi", 200, true);
            XRL.Core.XRLCore.Core.Game.PlayerReputation.set("Fungi", 200);
        }
        return base.Mutate(GO, Level);
      }

      public override bool Unmutate(GameObject GO)
      {
        RemoveMyActivatedAbility(ref StrainActivatedAbilityID);
  			if (Level < 1)
  			{
  						Popup.Show("The fungi strain has been cleansed from your body.");
  			}
        return base.Unmutate(GO);
      }
    }
}
